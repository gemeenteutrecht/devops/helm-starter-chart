# Helm Starter Chart

This [starter chart](https://helm.sh/docs/topics/charts/#chart-starter-packs) can be used when running `helm create` and provides several additional features on top of the default base templates: 

* support for a ConfigMap and Secret
* bring your own secret to avoid managing secrets through Helm
* add additional pod labels

<br>

[[_TOC_]]

<br>

## Usage
In order to generate a Helm chart based on this starter chart, execute the following steps:

In your homedir or any other preferred location, create these folders:
```
mkdir -p ~/helm/starters
```

Export the `XDG_DATA_HOME` environment variable through your shell's RC file (bashrc/zshrc etc). 
E.g. if you have created the `helm/starters` folders in your homedir, then set the `XDG_DATA_HOME` 
to your homedir location. You need to use absolute paths, so if your homedir is located in `/Users/jordi`, 
you set the following: 
```
export XDG_DATA_HOME="/Users/jordi"
```

Clone this repo in the `helm/starters` directory:
```
cd ~/helm/starters
git clone https://gitlab.com/gemeenteutrecht/devops/helm-starter-chart
```

Generate a new Helm chart based of the starter chart:
```
helm create my-app --starter helm-starter-chart
```

__*Optional:*__ 
 
You can rename the starter chart to anything you'd like:
```
mv helm-starter-chart gu-starter-chart
helm create my-app --starter gu-starter-chart
```  
<br>
<br>

## Caveats
* When referring to your own secret, the pods are not restarted when you make a change in the concerning secret, hence you should `rollout restart` the deployment yourself. 